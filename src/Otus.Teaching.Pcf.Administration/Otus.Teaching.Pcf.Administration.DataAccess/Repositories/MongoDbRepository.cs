using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbRepository<T> : IRepository<T>
        where T: BaseEntity
    {
        private readonly IMongoCollection<T> _collection;

        public MongoDbRepository(IMongoDatabase database)
        {
            _collection = database.GetCollection<T>(typeof(T).ToString());
        } 
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return (await _collection.FindAsync(FilterDefinition<T>.Empty)).ToEnumerable();
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, id);
            return _collection.Find(filter).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var filter = Builders<T>.Filter.Where(s=>ids.Contains(s.Id));
            return (await  _collection.FindAsync(filter)).ToEnumerable();
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
           return _collection.Find(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return (await _collection.FindAsync(predicate)).ToEnumerable();
        }

        public  async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, entity.Id);
            await _collection.FindOneAndReplaceAsync(filter, entity);
        }

        public async Task DeleteAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(doc => doc.Id, entity.Id);
            await _collection.FindOneAndDeleteAsync(filter);
        }
    }
}