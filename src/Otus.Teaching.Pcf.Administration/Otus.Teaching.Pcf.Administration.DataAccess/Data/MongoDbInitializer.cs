using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class  MongoDbInitializer :IDbInitializer
    {
        private readonly IMongoDatabase _dataContext;

        public MongoDbInitializer(IMongoDatabase dataContext)
        {
            _dataContext = dataContext;
        }
        public void InitializeDb()
        {
            foreach (var col in _dataContext.ListCollections().ToEnumerable())
            {
                if(col.TryGetValue("name", out var value))
                    _dataContext.DropCollection(value.ToString());
            }
            // _dataContext.DropCollection(typeof(Employee).ToString());
            //_dataContext.DropCollection(typeof(Role).ToString());
            _dataContext.CreateCollection(typeof(Employee).ToString());
            _dataContext.CreateCollection(typeof(Role).ToString());
            _dataContext.GetCollection<Employee>(typeof(Employee).ToString()).InsertMany(FakeDataFactory.Employees);
            _dataContext.GetCollection<Role>(typeof(Role).ToString()).InsertMany(FakeDataFactory.Roles);
        }
    }
}