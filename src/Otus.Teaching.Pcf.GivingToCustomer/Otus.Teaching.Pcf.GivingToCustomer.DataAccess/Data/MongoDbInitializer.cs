using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class  MongoDbInitializer :IDbInitializer
    {
        private readonly IMongoDatabase _dataContext;

        public MongoDbInitializer(IMongoDatabase dataContext)
        {
            _dataContext = dataContext;
        }
        public void InitializeDb()
        {
            foreach (var col in _dataContext.ListCollections().ToEnumerable())
            {
                if(col.TryGetValue("name", out var value))
                    _dataContext.DropCollection(value.ToString());
            }
            _dataContext.CreateCollection(typeof(Preference).ToString());
            _dataContext.CreateCollection(typeof(Customer).ToString());
            _dataContext.GetCollection<Preference>(typeof(Preference).ToString()).InsertMany(FakeDataFactory.Preferences);
            _dataContext.GetCollection<Customer>(typeof(Customer).ToString()).InsertMany(FakeDataFactory.Customers);
        }
    }
}